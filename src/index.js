import React, { useState } from "react";
import ReactDOM from "react-dom";
import "./styles.css";
import {
  createMachine,
  state,
  transition,
  guard,
  immediate,
  reduce,
  invoke
} from "robot3";
import { useMachine } from "react-robot";

const isAValidEmail = ({ email }) => email !== "";

const sendToServer = () => {
  return new Promise(r => {
    setTimeout(() => r(), 2000);
  });
};

const waitFiveSeconds = () => {
  return new Promise(r => {
    setTimeout(() => r(), 5000);
  });
};

const machine = createMachine("invalid", {
  invalid: state(
    transition(
      "submit",
      "validation",
      // sets a new context object
      reduce((ctx, ev) => ({
        ...ctx,
        email: ev.data.email
      }))
    )
  ),
  validation: state(
    immediate("sending", guard(isAValidEmail)),
    immediate("invalid")
  ),
  sending: invoke(
    sendToServer,
    transition("done", "thankyou"),
    transition("error", "error")
  ),
  thankyou: state(),
  error: invoke(waitFiveSeconds, transition("done", "invalid"))
});

function ContactUs() {
  const [email, setEmail] = useState("");

  const [current, send] = useMachine(machine, () => ({
    email: "" // sets initial context
  }));

  // when in the invalid state show red borders
  const borderColor = current.name === "invalid" ? "red" : "green";

  const handleSubmit = e => {
    e.preventDefault();
    // send "submit" and merge email into the
    // machine's context
    send({ type: "submit", data: { email } });
  };

  return (
    <div>
      {current.name === "thankyou" ? (
        <h3>Thank you for contacting us!</h3>
      ) : (
        <form onSubmit={handleSubmit}>
          {current.name === "sending" && <p>Sending...</p>}
          {current.name === "error" && <p>Something went wrong :(</p>}
          {current.name === "invalid" && (
            <p>
              <small>Please provide a valid email</small>
            </p>
          )}
          <input
            type="email"
            name="email"
            value={email}
            onChange={e => setEmail(e.currentTarget.value)}
            placeholder="Your Email"
            style={{ borderColor }}
          />
          <button type="submit">Submit</button>
        </form>
      )}
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <ContactUs />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
